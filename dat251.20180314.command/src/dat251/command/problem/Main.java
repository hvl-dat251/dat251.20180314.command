package dat251.command.problem;

public class Main {

	public static void main(String[] args) {

		FrontController fc = new FrontController();

		fc.service(Requests.HELLO_REQUEST);
		fc.service(Requests.DIE_REQUEST);
		fc.service(Requests.TIME_REQUEST);
		fc.service(Requests.GOODBYE_REQUEST);
	}

}
