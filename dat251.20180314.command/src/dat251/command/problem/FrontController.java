package dat251.command.problem;

import java.util.Date;

public class FrontController {

	public void service(String request) {

		if (request.equals(Requests.HELLO_REQUEST)) {
			doHello();

		} else if (request.equals(Requests.TIME_REQUEST)) {
			doTime();

		} else if (request.equals(Requests.DIE_REQUEST)) {
			doDie();

		} else {
			doUnknownRequest(request);
		}
	}

	private void doHello() {
		System.out.println("Hello");
	}

	private void doDie() {
		System.out.println((int) (Math.random() * 6 + 1));
	}

	private void doTime() {
		System.out.println(new Date());
	}

	private void doUnknownRequest(String request) {
		System.out.println("Unknown request: " + request);
	}

}
