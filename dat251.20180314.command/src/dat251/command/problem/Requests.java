package dat251.command.problem;

public class Requests {
	
	public static final String HELLO_REQUEST = "Hello";
	public static final String TIME_REQUEST = "Time";
	public static final String DIE_REQUEST = "Die";
	public static final String GOODBYE_REQUEST = "Goodbye";

}
